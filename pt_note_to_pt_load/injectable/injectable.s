BITS 64

; compile using nasm -f bin injectable.txt

jmp _inf_start
msg	db	"uwu rawr!", 0x0a

_inf_start:
push rdx		; rdx normally doesn't need to be preserved, but since we are directly
			; calling _start after this, and _start directly loads shit from rdx,
			; we kinda need to preserve it.
			; (or that's my guess, cause the program was SEGFAULTing without it)

mov rax, 1		; write()
mov rdi, 1		; STDOUT
lea rsi, [rel msg]	; relative load msg address
mov rdx, 0x0a		; msg size
syscall

pop rdx
