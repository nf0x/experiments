/*
 * Small module to export kallsyms_lookup_name on kernels post 5.7.0
 * Author: netfox (me@ghikio.dev)
 * License: IDGAF
 */

#ifndef EXP_KALLSYMS_H
#define EXP_KALLSYMS_H

#include <linux/version.h>


/* From Linux 5.7.0 onwards, kallsyms_lookup_name is not longer exported by default.
 * The workaround is to launch a probe that looks up for the function on the kernel
 * memory space and returns its address. */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 7, 0)

#define KPROBE_LOOKUP 1
#include <linux/kprobes.h>

#endif

/* Lookup the address for this symbol. Returns 0 if not found. */
unsigned long exp_kallsyms_lookup_name(const char *name);


#endif /* EXP_KALLSYMS_H */
