/*
 * Small library to export kallsyms_lookup_name on kernels post 5.7.0
 * Author: netfox (say-hi@netfox.rip)
 * License: IDGAF
 *
 * Based on https://github.com/xcellerator/linux_kernel_hacking/issues/3
 */


#include "exp_kallsyms.h"

#if KPROBE_LOOKUP

static struct kprobe kp = {
    .symbol_name = "kallsyms_lookup_name"
};

#endif

typedef unsigned long (*kallsyms_lookup_name_t)(const char *name);
kallsyms_lookup_name_t __kallsyms_lookup_name;

/* Fetches the function `kallsyms_lookup_name`, looking it up from
 * kernel memory if needed. */
static kallsyms_lookup_name_t export_kallsyms_lookup_name(void)
{
    if (__kallsyms_lookup_name)
	return __kallsyms_lookup_name;

#if KPROBE_LOOKUP
    register_kprobe(&kp);
    __kallsyms_lookup_name = (kallsyms_lookup_name_t) kp.addr;
    unregister_kprobe(&kp);
#else
    __kallsyms_lookup_name = kallsyms_lookup_name;
#endif

    return __kallsyms_lookup_name;
}

/* Lookup the address for this symbol. Returns 0 if not found. */
unsigned long exp_kallsyms_lookup_name(const char *name)
{
    return export_kallsyms_lookup_name()(name);
}
